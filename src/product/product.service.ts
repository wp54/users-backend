import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

let products: Product[] = [
  { id: 1, name: 'Phoneeee', price: 1000 },
  { id: 2, name: 'cookie', price: 50 },
  { id: 3, name: 'computer', price: 50000 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDtoDto: UpdateProductDto) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    //console.log('user' + JSON.stringify(users[index]));
    //console.log('update' + JSON.stringify(updateUserDto));
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDtoDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((product) => {
      return product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deleteProduct = products[index];
    products.splice(index, 1);
    return deleteProduct;
  }
  reset() {
    products = [
      { id: 1, name: 'Phoneeee', price: 1000 },
      { id: 2, name: 'cookie', price: 50 },
      { id: 3, name: 'computer', price: 50000 },
    ];
    const lastProductId = 4;
    return 'RESET';
  }
}
